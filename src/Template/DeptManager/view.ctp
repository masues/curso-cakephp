<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Department Manager'), 
            ['action' => 'edit', $deptManager->emp_no,$deptManager->dept_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Department Manager'),
            ['action' => 'delete', $deptManager->emp_no,$deptManager->dept_no], 
            ['confirm' => __('Are you sure you want to delete emp_no={0}, dept_no={1}?',
                $deptManager->emp_no,$deptManager->dept_no)]) ?> </li>
        <li><?= $this->Html->link(__('List Department Manager'),
            ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Department Manager'),
            ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deptManager view large-9 medium-8 columns content">
    <h3><?= h($deptManager->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dept No') ?></th>
            <td><?= h($deptManager->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Emp No') ?></th>
            <td><?= $this->Number->format($deptManager->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('From Date') ?></th>
            <td><?= h($deptManager->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('To Date') ?></th>
            <td><?= h($deptManager->to_date) ?></td>
        </tr>
    </table>
</div>
