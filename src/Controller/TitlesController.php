<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Titles Controller
 *
 * @property \App\Model\Table\TitlesTable $Titles
 *
 * @method \App\Model\Entity\Title[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TitlesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $titles = $this->paginate($this->Titles);

        $this->set(compact('titles'));
    }

    /**
     * View method
     *
     * @param string|null $id Id del título.
     * @param string|null $title Nombre del título
     * @param string|null $from_date Fecha del título
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null,$title = null, $from_date = null)
    {
        $title = $this->Titles->get([$id,$title,$from_date]);

        $this->set('title', $title);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $title = $this->Titles->newEntity();
        if ($this->request->is('post')) {
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('El título ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->
                error(__('El título no pudo ser guardado. Por favor, intenta de nuevo'));
        }
        $this->set(compact('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Id del título.
     * @param string|null $title Nombre del título
     * @param string|null $from_date Fecha del título
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $title=null, $from_date=null)
    {
        $title = $this->Titles->get([$id,$title,$from_date]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            if ($this->Titles->save($title)) {
                $this->Flash->success(__('El título ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                __('El título no pudo ser guardado. Por favor, intente de nuevo.'));
        }
        $this->set(compact('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Id del título.
     * @param string|null $title Nombre del título
     * @param string|null $from_date Fecha del título
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $title=null, $from_date=null)
    {
        //Restricción de los tipos de peticiones
        $this->request->allowMethod(['post', 'delete']);
        //Obtención de la información de un registro dependiendo de sus llaves primarias
        $title = $this->Titles->get([$id,$title,$from_date]);
        if ($this->Titles->delete($title)) {
            $this->Flash->success(__('El título ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El título no pudo ser eliminado. Por favor, intente de nuevo.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /** 
     * Método que inicializa el controlador
     * Este método se ejecuta antes que cualquier otro.
     * @return \Cake\Http\Response|null
    */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Employees');

    }

     /**
     *
     * Método para ver todos los registros de los títulos en dónde el empleado
     * del título es mujer
     * @return \Cake\Http\Response|null
     * 
    */ 
    public function listaMujeres()
    {   
        //Se buscan los títulos que sean de empleados mujeres
        $query = $this->Titles->find()
            ->contain('Employees')
            ->where(['Employees.gender'=>'F']);
        //Se manda la información al compnente para que sepa cómo mostrar los datos
        $titulosMujeres= $this->paginate($query);
        //Se manda toda la infrmación ya paginada a la vista
        $this->set(compact('titulosMujeres'));
        
      /*   $query = $this->Employees->find()
            ->contain('Titles')
            ->where(['Employees.gender'=>'M'])
            ->limit(10);
        sql($query);
        debug($query->enableHydration(false)->toList());
        exit; */
    } 
}

