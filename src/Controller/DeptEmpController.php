<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptEmp Controller
 *
 * @property \App\Model\Table\DeptEmpTable $DeptEmp
 *
 * @method \App\Model\Entity\DeptEmp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptEmpController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptEmp = $this->paginate($this->DeptEmp);

        $this->set(compact('deptEmp'));
    }

    /**
     * View method
     *
     * @param string|null $emp_no, $dept_no Dept Emp id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($emp_no = null, $dept_no = null)
    {
        $deptEmp = $this->DeptEmp->get([$emp_no, $dept_no]);

        $this->set('deptEmp', $deptEmp);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deptEmp = $this->DeptEmp->newEntity();
        if ($this->request->is('post')) {
            $deptEmp = $this->DeptEmp->patchEntity($deptEmp, $this->request->getData());
            if ($this->DeptEmp->save($deptEmp)) {
                $this->Flash->success(__('The dept emp has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dept emp could not be saved. Please, try again.'));
        }
        $this->set(compact('deptEmp'));
    }

    /**
     * Edit method
     *
     * @param string|null $emp_no, $dept_no Dept Emp id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($emp_no = null, $dept_no = null)
    {
        $deptEmp = $this->DeptEmp->get([$emp_no, $dept_no]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //Creación de una nueva entidad para suplantar la anterior
            $newDeptEmp = $this->DeptEmp->newEntity();
            
            //obtiene los datos ingresados
            $newDeptEmp = $this->DeptEmp->patchEntity($newDeptEmp, $this->request->getData());
            //Si logro guardar los datos
            if ($this->DeptEmp->save($newDeptEmp)) {
                //Envia un mensaje de éxtio
                $this->Flash->success(__('The dept emp has been saved.'));
                //Elimina el registro anterior
                $this->DeptEmp->delete($deptEmp);
                //Redirige al listado de Department Employees
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dept emp could not be saved. Please, try again.'));
        }
        $this->set(compact('deptEmp'));
    }

    /**
     * Delete method
     *
     * @param string|null $emp_no, $dept_no Dept Emp id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($emp_no = null,  $dept_no=null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Se obtienen la información de acuerdo a las llaves primarias
        $deptEmp = $this->DeptEmp->get([$emp_no, $dept_no]);
        if ($this->DeptEmp->delete($deptEmp)) {
            $this->Flash->success(__('The dept emp has been deleted.'));
        } else {
            $this->Flash->error(__('The dept emp could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
