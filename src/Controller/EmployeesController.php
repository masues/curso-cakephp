<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Método que inicializa el controlador
     */
    public function initialize(){
        parent::initialize();
        //Carga al modelo Salaries para poder hacer la
        //consulta iniciando desde esta tabla.
        $this->loadModel('Salaries');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $employees = $this->paginate($this->Employees);

        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        $this->set('employee', $employee);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Consulta para obtener el  mayor id
        $query = $this->Employees
            ->find();
        $query->select(['emp_no' => $query->func()->max('emp_no')]);
       
        $queryList=$query->enableHydration(false)->toList();

        //Salva el valor del mayor id encontrado
        $emp_no = $queryList[0]["emp_no"];
        
        //Incrementa su valor
        $emp_no++;
        
        $employee = $this->Employees->newEntity();
        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Le añade al nuevo usuario el id encontrado previamente
            $employee->emp_no=$emp_no;
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Método para eliminar un registro de la tabla employee
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The employee has been deleted.'));
        } else {
            $this->Flash->error(__('The employee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * Método login
     */
    public function login()
    {
        if ($this->request->is('post')){
            $employee = $this -> Auth -> identify();
            if ($employee){
                $this -> Auth -> setUser($employee);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(_('Invalid username or password, try again.'));
        }
    }

    /**
     * Método para cerrar sesión
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

     /**
     * Método para cambiar la contraseña del employee
     *
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function changePass()
    {   
        //En vez de recibir el id de alguna vista, lo lee de la sesión activa
        $id= $this->getRequest()->getSession()->read('Auth.User.emp_no');
        //También obtiene el email de la sesión activa y lo guarda en una variable
        //puesto que  al momento de cambiar la contraseña, solicita al email
        //como campo obligatorio
        $email=$this->getRequest()->getSession()->read('Auth.User.email');

        //Obtiene al registro con el id de la sesión actual
        $employee = $this->Employees->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //A la variable employee, se le colocan los atributos recogidos
            //desde la vista.
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());

            //Coloca el email salvado previamente
            $employee->email=$email;

            //Trata de guardar el registro
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The password has been changed.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The password could not be changed.'));
        }
        $this->set(compact('employee'));
    }

    /**
     * Método Query. Lista a los empleados del departamento de Finance con un salario mayor a 100,000
     */
    public function queryEmp()
    {   
        //Realiza la consulta
        $employees = $this->Salaries
            ->find()
            ->select(['emp_no' => 'Employees.emp_no',
                'birth_date' => 'Employees.birth_date',
                'first_name'=> 'Employees.first_name',
                'last_name'=> 'Employees.last_name',
                'gender'=> 'Employees.gender',
                'hire_date'=> 'Employees.hire_date',
                'email'=> 'Employees.email',
                'password'=> 'Employees.password'
            ])
            ->innerJoinWith('Employees.Dept_Emp')
            ->where(['Dept_Emp.dept_no' => 'd002',//d002 es el departamento de 'finance'
            'Salaries.salary >' => 100000]);

        //Manda la infomramción al paginador para que sepa como mostrar los datos
        $listadoEmployees = $this->paginate($employees);
        //Envia la información paginada a la vista
        $this->set(compact('listadoEmployees'));
    }
    
  
}
