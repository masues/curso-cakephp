<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptManager Controller
 *
 * @property \App\Model\Table\DeptManagerTable $DeptManager
 *
 * @method \App\Model\Entity\DeptManager[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptManagerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptManager = $this->paginate($this->DeptManager);

        $this->set(compact('deptManager'));
    }

    /**
     * View method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($emp_no=null,$dept_no=null)
    {
        $deptManager = $this->DeptManager->get([$emp_no,$dept_no]);

        $this->set('deptManager', $deptManager);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deptManager = $this->DeptManager->newEntity();
        if ($this->request->is('post')) {
            $deptManager = $this->DeptManager->patchEntity($deptManager, $this->request->getData());
            if ($this->DeptManager->save($deptManager)) {
                $this->Flash->success(__('The dept manager has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dept manager could not be saved. Please, try again.'));
        }
        $this->set(compact('deptManager'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($emp_no=null,$dept_no=null)
    {
        $deptManager = $this->DeptManager->get([$emp_no,$dept_no]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //Creación de una nueva entidad para suplantar la anterior
            $newDeptManager = $this->DeptManager->newEntity();

            //obtiene los datos ingresados
            $newDeptManager = $this->DeptManager->patchEntity($newDeptManager, $this->request->getData());

            //Si logra salvar los datos
            if ($this->DeptManager->save($newDeptManager)) {
                //Envia un mensaje de éxtio
                $this->Flash->success(__('The dept manager has been saved.'));
                //Elimina el registro anterior
                $this->DeptManager->delete($deptManager);
                //Redirige al listado de Department Employees
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dept manager could not be saved. Please, try again.'));
        }
        $this->set(compact('deptManager'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($emp_no=null,$dept_no=null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deptManager = $this->DeptManager->get([$emp_no,$dept_no]);
        if ($this->DeptManager->delete($deptManager)) {
            $this->Flash->success(__('The dept manager has been deleted.'));
        } else {
            $this->Flash->error(__('The dept manager could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
