<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeptManager Model
 *
 * @method \App\Model\Entity\DeptManager get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeptManager newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeptManager[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeptManager|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeptManager saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeptManager patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeptManager[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeptManager findOrCreate($search, callable $callback = null, $options = [])
 */
class DeptManagerTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dept_manager');
        $this->setDisplayField('emp_no');
        $this->setPrimaryKey(['emp_no', 'dept_no']);

        //Asociaciones
        $this->belongsTo('employees')->setForeignKey('emp_no');
        $this->belongsTo('departments')->setForeignKey('emp_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('emp_no')
            ->requirePresence('emp_no');

        $validator
            ->scalar('dept_no')
            ->maxLength('dept_no', 4)
            ->requirePresence('dept_no');

        $validator
            ->date('from_date')
            ->requirePresence('from_date')
            ->notEmptyDate('from_date');

        $validator
            ->date('to_date')
            ->requirePresence('to_date', 'create')
            ->notEmptyDate('to_date');

        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {   
        //Valida que la llave primaria sea única
        $rules->add(
            $rules->isUnique(
                ['emp_no','dept_no'],
                'LLave primaria ya existente en la BD'
            )
        );
        //Valida que la llave foránea exista
        $rules->add(
            $rules->existsIn(
                ['emp_no'],
                'employees',
                'LLave foránea no encontrada'
            )
        );
        //Valida que la llave foránea exista
        $rules->add(
            $rules->existsIn(
                ['dept_no'],
                'departments',
                'LLave foránea no encontrada'
            )
        );
        return $rules;
    }
}
